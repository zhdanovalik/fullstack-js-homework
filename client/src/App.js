import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchWords } from './actions';
import debounce from 'lodash.debounce';

import Container from './components/common/Container';
import Grid from './components/common/Grid';
import GridItem from './components/common/GridItem';
import WordsList from './components/WordsList';
import Keyboard from './components/Keyboard';
import WordInput from './components/WordInput';

const Header = styled.header`
  margin-bottom: 1em;
  
  h1 {
    margin-bottom: 0;
  }
`;

const WordsAmount = styled.div`
  font-size: 1.25em;
  text-align: right;
  margin-bottom: 0.75em;
`;


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputNumber: '',
      listOfWords: []
    };

    this.onKeyboardInput = this.onKeyboardInput.bind(this);
    this.onKeyboardDelete = this.onKeyboardDelete.bind(this);
    this.fetchWordsThrottled = debounce(this.fetchWords, 500)
  }

  onKeyboardInput(value) {
    this.setInputNumber((oldVal) => {
      return oldVal += value
    })
  }

  onKeyboardDelete() {
    this.setInputNumber((oldVal) => {
      return oldVal.slice(0, -1)
    })
  }

  setInputNumber(fn) {
    this.setState((prevState) => {
      const inputNumber = fn(prevState.inputNumber);

      this.fetchWordsThrottled(inputNumber);

      return {inputNumber}
    })
  }

  fetchWords(inputNumber) {
    this.props.dispatch(fetchWords(inputNumber))
  }

  render() {
    const { listOfWords, wordsIsFetching, wordsDidInvalidate } = this.props;
    const { inputNumber } = this.state;

    return (
      <div className="App">
        <Container>
          <Header>
            <h1>Fullstack JS Homework</h1>
            <span>by Ali Zhdanov</span>
          </Header>

          <Grid>
            <GridItem span={6}>
              <WordInput input={inputNumber}
                         loading={wordsIsFetching}
                         onDelete={this.onKeyboardDelete}  />

              <Keyboard onInput={this.onKeyboardInput} />
            </GridItem>
            <GridItem span={6}>
                <WordsAmount><strong>Total words:</strong> {listOfWords.length}</WordsAmount>
                <WordsList listOfWords={listOfWords}
                           loading={wordsIsFetching}
                           error={wordsDidInvalidate} />
            </GridItem>
          </Grid>
        </Container>
      </div>
    );
  }
}

App.propTypes = {
  listOfWords: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired,
  wordsIsFetching: PropTypes.bool.isRequired,
  wordsDidInvalidate: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => {
  return {
    listOfWords: state.words.items,
    wordsIsFetching: state.words.isFetching,
    wordsDidInvalidate: state.words.didInvalidate
  }
};

export default connect(mapStateToProps)(App);

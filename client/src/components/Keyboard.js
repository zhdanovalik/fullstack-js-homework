import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { brandColor, greyPallete } from '../styles/variables'

import UnstyledButton from './common/UnstyledButton'

const buttonsList = [
  { name: '1', label: '', disabled: true },
  { name: '2', label: 'abc', disabled: false },
  { name: '3', label: 'def', disabled: false },
  { name: '4', label: 'ghi', disabled: false },
  { name: '5', label: 'jkl', disabled: false },
  { name: '6', label: 'mno', disabled: false },
  { name: '7', label: 'pqrs', disabled: false },
  { name: '8', label: 'tuv', disabled: false },
  { name: '9', label: 'wxyz', disabled: false },
  { name: '*', label: '', disabled: true },
  { name: '0', label: '', disabled: true },
  { name: '#', label: '', disabled: true },
];

const Wrap = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const Button = styled(UnstyledButton)`
  padding: 0.25em;
  flex: 1 1 33%;
  max-width: 33%; // for IE
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  
  &[disabled] {
    color: ${greyPallete.borderColor};
    
    .name {
      color: ${greyPallete.borderColor};
    }
  }
  
  .name {
    font-size: 2em;
    color: ${brandColor};
  }
  
  .label {
    letter-spacing: 0.15em;
  }
`;

const KeyBoard = ({onInput}) => (
  <Wrap>
    {buttonsList.map(({name, disabled, label}) =>
      <Button key={name}
              disabled={disabled}
              onClick={() => {onInput(name)}}>
        <div className="name">{name}</div>
        {label.length > 0 && <span className="label">{label}</span>}
      </Button>
    )}
  </Wrap>
);

KeyBoard.propTypes = {
  onInput: PropTypes.func.isRequired
};

export default KeyBoard
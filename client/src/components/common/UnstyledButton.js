import React from 'react';
import styled from 'styled-components';

const Button = styled.button`
  padding: 0;
  background: none;
  border: 0;
  border-radius: 0;
  box-shadow: none;
  
  &:not([disabled]) {
    cursor: pointer;
  }
`;

const UnstyledButton = (props) => (
  <Button {...props} />
);

export default UnstyledButton
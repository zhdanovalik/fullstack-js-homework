import React from 'react';
import styled from 'styled-components';

const Wrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -0.5rem;
`;

const Grid = (props) => (
  <Wrap {...props} />
);

export default Grid
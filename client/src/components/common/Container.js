import React from 'react';
import styled from 'styled-components';

const Wrap = styled.div`
  max-width: 980px;
  margin: 0 auto;
  padding: 0 20px;
`;

const Container = (props) => (
  <Wrap {...props} />
);

export default Container
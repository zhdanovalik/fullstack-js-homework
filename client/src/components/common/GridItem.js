import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import {
  smBreakPoint
} from '../../styles/variables'

const Wrap = styled.div`
  padding: 0 0.5rem 1rem;
  flex-basis: 100%;
  max-width: 100%;
  
  @media (min-width: ${smBreakPoint}) {
    flex: 1 1 50%;
    max-width: ${props => props.span / 12 * 100}%;
  }
`;

const GridItem = (props) => (
  <Wrap {...props} />
);

GridItem.propTypes = {
  span: PropTypes.number.isRequired
};

GridItem.defaultProps = {
  span: 1
};

export default GridItem
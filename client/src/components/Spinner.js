import React from 'react';
import styled, { keyframes } from 'styled-components';

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const Wrapper = styled.div`
  width: 1em;
  height: 1em;
  border: 1px solid;
  border-bottom: 0;
  border-radius: 50%;
  animation: ${rotate360} 0.75s linear infinite;;
`;

const Spinner = (props) => (
  <Wrapper {...props} />
);

export default Spinner
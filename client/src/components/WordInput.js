import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { greyPallete } from '../styles/variables';

import Spinner from './Spinner';
import UnstyledButton from './common/UnstyledButton';

const Wrap = styled.div`
  position: relative;
`;

const ControlsWrap = styled.div`
  display: flex;
  position: absolute;
  right: 1em;
  top: 50%;
  transform: translateY(-50%);
`;

const Input = styled.input`
  -webkit-appearance: none;
  font-size: 1rem;
  height: 2.5em;
  padding: 0.35em 0.7em;
  width: 100%;
  border: 1px solid ${greyPallete.borderColor};
  border-radius: 3px;
  box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.1);
  background-color: ${greyPallete.background};
`;

const DeleteButton = styled(UnstyledButton)`
  margin-left: 0.5em;
  font-size: 1rem;
  
  svg {
    width: 1em;
    height: 1em;
    fill: currentColor;
  }
`;


const WordInput = ({input, loading, onDelete}) => (
  <Wrap>
    <Input value={input}
           disabled/>
    <ControlsWrap>
      {loading && <Spinner /> }
      <DeleteButton onClick={onDelete}>
        <svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
          <path d="M1664 896v128q0 53-32.5 90.5t-84.5 37.5h-704l293 294q38 36 38 90t-38 90l-75 76q-37 37-90 37-52 0-91-37l-651-652q-37-37-37-90 0-52 37-91l651-650q38-38 91-38 52 0 90 38l75 74q38 38 38 91t-38 91l-293 293h704q52 0 84.5 37.5t32.5 90.5z"/>
        </svg>
      </DeleteButton>
    </ControlsWrap>
  </Wrap>
);

WordInput.propTypes = {
  input: PropTypes.string.isRequired,
  loading: PropTypes.bool,
  onDelete: PropTypes.func.isRequired
};

export default WordInput
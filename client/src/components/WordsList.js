import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { greyPallete } from '../styles/variables';

const Wrap = styled.div`
  position: relative;
`;

const Overlay = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background-color: rgba(255, 255, 255, 0.75);
  
  // transition related styles
  opacity: 0;
  visibility: hidden;
  transition: opacity 0.2s ease, visibility 0.2s ease;
  
  &.visible {
    visibility: visible;
    opacity: 1;
  }
`;

const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
  background-color: ${greyPallete.background};
  color: ${greyPallete.color};
  border-radius: 0.125rem;
  border: 1px solid ${greyPallete.borderColor};
`;

const Item = styled.li`
  padding: 1em;
  
  &:not(:last-child) {
    border-bottom: 1px solid ${greyPallete.borderColor};
  }
`;

const WordsList = ({listOfWords, loading, error}) => {
  let listItems;
  if (error) {
      listItems = <Item><strong>Error occured.</strong></Item>
  } else if (listOfWords.length > 0) {
    listItems = listOfWords.map(word => (
      <Item key={word}>{word}</Item>
    ))
  } else {
    listItems = <Item><strong>No results</strong></Item>
  }

  return (
    <Wrap>
      <List>
        {listItems}
      </List>
      <Overlay className={loading ? 'visible' : ''} />
    </Wrap>
  )
};

WordsList.propTypes = {
  listOfWords: PropTypes.array.isRequired,
  loading: PropTypes.bool,
  error: PropTypes.bool
};

export default WordsList
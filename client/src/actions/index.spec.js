import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);


import {
    INVALIDATE_WORDS_REQUEST,
    REQUEST_WORDS,
    RECIEVE_WORDS,
    CACHE_WORDS,
    invalidateWordsRequest,
    fetchWords,
    cacheWords
} from './index'

describe('word actions', () => {
    it('creates INVALIDATE_WORDS_REQUEST action', () => {
        expect(invalidateWordsRequest()).toEqual({
            type: INVALIDATE_WORDS_REQUEST
        })
    });

    it('creates CACHE_WORDS action', () => {
        const response = {
            count: 9,
            items: [
                'ad', 'ae', 'af',
                'bd', 'be', 'bf',
                'cd', 'ce', 'cf'
            ],
            query: '23'
        };

        expect(cacheWords(response)).toEqual({
            type: CACHE_WORDS,
            response
        })
    });

    describe('async actions', () => {
        const matcher = '/api/words?q=2';

        beforeEach(() => {
            fetchMock
                .getOnce(
                    matcher,
                    {
                        body: {
                            count: 3,
                            items: ['a', 'b', 'c',],
                            query: '2'
                        },
                        headers: {'content-type': 'application/json'}
                    }
                );
        });

        afterEach(() => {
            fetchMock.reset()
            fetchMock.restore()
        });

        it('fetching words from backend', () => {
            const expectedActions = [
                {type: REQUEST_WORDS},
                {
                    type: CACHE_WORDS,
                    response: {
                        count: 3,
                        items: ['a', 'b', 'c',],
                        query: '2'
                    }
                },
                {
                    type: RECIEVE_WORDS,
                    words: ['a', 'b', 'c']
                }
            ];

            const store = mockStore({
                words: {
                    items: [],
                    cachedResults: {}
                }
            });

            return store.dispatch(fetchWords('2')).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(fetchMock.called(matcher)).toBe(true);
            })
        });

        it('fetching words from cache', () => {
            const expectedActions = [
                {type: REQUEST_WORDS},
                {
                    type: RECIEVE_WORDS,
                    words: ['a', 'b', 'c']
                }
            ];

            const store = mockStore({
                words: {
                    items: [],
                    cachedResults: {
                        '2': {
                            count: 3,
                            items: ['a', 'b', 'c',],
                            query: '2'
                        }
                    }
                }
            });

            return store.dispatch(fetchWords('2')).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(fetchMock.called(matcher)).toBe(false);
            })
        });
    });
});
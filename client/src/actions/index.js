export const REQUEST_WORDS = 'REQUEST_WORDS';
export const RECIEVE_WORDS = 'RECIEVE_WORDS';
export const CACHE_WORDS = 'CACHE_WORDS';
export const INVALIDATE_WORDS_REQUEST = 'INVALIDATE_WORD_REQUEST';

export function invalidateWordsRequest() {
  return {
    type: INVALIDATE_WORDS_REQUEST
  }
}

function requestWords() {
  return {
    type: REQUEST_WORDS
  }
}

function receiveWords(items) {
  return {
    type: RECIEVE_WORDS,
    words: items
  }
}

export function cacheWords(response) {
  return {
    type: CACHE_WORDS,
    response
  }
}

export function fetchWords(query) {
  return (dispatch, getState) => {
    dispatch(requestWords());

    const cache = getState().words.cachedResults;
    const cachedResult = cache[query];
    let request;

    // We're checking if words exists in cache
    // and loading them from cache or fetching them from backend

    if (cachedResult) {
      request = Promise.resolve(cachedResult.items)
    } else {
      request = fetch(`/api/words?q=${query}`)
        .then(res => res.json())
        .then((response) => {
          dispatch(cacheWords(response));
          return response.items
        })
    }
    return request
      .then(items => dispatch(receiveWords(items)))
      .catch(err => dispatch(invalidateWordsRequest()))
  }
}


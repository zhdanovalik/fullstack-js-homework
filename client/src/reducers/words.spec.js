import words, {initialState} from './words';
import {
    INVALIDATE_WORDS_REQUEST,
    REQUEST_WORDS,
    RECIEVE_WORDS,
    CACHE_WORDS
} from "../actions";

describe('words reducer', () => {
    it('should handle INVALIDATE_WORDS_REQUEST', () => {
        const expected = Object.assign(
          {},
          initialState,
          { didInvalidate: true }
        );

        const result = words(undefined, {
            type: INVALIDATE_WORDS_REQUEST
        });

        expect(result).toEqual(expected);
    });

    it('should handle REQUEST_WORDS', () => {
        const expected = Object.assign(
            {},
            initialState,
            { isFetching: true }
        );

        const result = words(undefined, {
            type: REQUEST_WORDS
        });

        expect(result).toEqual(expected);
    });

    it('should handle RECIEVE_WORDS', () => {
      const expected = Object.assign(
        {},
        initialState,
        {
          items: ['a', 'b', 'c']
        }
      );

        const result = words(undefined, {
            type: RECIEVE_WORDS,
            words: ['a', 'b', 'c']
        });

        expect(result).toEqual(expected);
    });

  it('should handle CACHE_WORDS', () => {
    const expected = Object.assign(
      {},
      initialState,
      {
        cachedResults: {
            '2': {
              count: 3,
              items: ['a', 'b', 'c'],
              query: '2'
            }
        }
      }
    );

    const result = words(undefined, {
      type: CACHE_WORDS,
      response: {
        count: 3,
        items: ['a', 'b', 'c'],
        query: '2'
      }
    });

    expect(result).toEqual(expected);
  });
});
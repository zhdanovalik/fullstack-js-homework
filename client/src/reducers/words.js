import {
    RECIEVE_WORDS,
    REQUEST_WORDS,
    INVALIDATE_WORDS_REQUEST,
    CACHE_WORDS
} from '../actions';

export const initialState = {
  isFetching: false,
  didInvalidate: false,
  items: [],
  cachedResults: {},
};


export function words(state = initialState, action) {
    switch (action.type) {
        case INVALIDATE_WORDS_REQUEST:
            return Object.assign({}, state, {
                didInvalidate: true,
                isFetching: false,
            });
        case REQUEST_WORDS:
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case RECIEVE_WORDS:
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                items: action.words,
            });
      case CACHE_WORDS:
        return Object.assign({}, state, {
          cachedResults: Object.assign({}, state.cachedResults, {
              [action.response.query]: action.response
          })
        });
        default:
            return state
    }
}

export default words
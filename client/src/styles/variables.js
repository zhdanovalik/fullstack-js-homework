export const smBreakPoint = '768px';

export const brandColor = '#200f80';

export const greyPallete = {
  color: '#555',
  borderColor: '#dcdcdc',
  background: 'whitesmoke'
};
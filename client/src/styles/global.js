import {injectGlobal} from 'styled-components';

const Style = injectGlobal`
  *,
  *::before,
  *::after {
      box-sizing: border-box;
  }
  
  body {
      margin: 0;
      padding: 0;
      font-family: sans-serif;
  }
`;

export default Style
var express = require('express');
var path = require('path');

var api = require('./routes/api');

var app = express();

app.use('/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // render the error page
  res.status(err.status || 500);
  res.json({
      code: err.status || 500,
      message: res.locals.message
  });
});

module.exports = app;

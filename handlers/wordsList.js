// TODO: add dests

const digits = {
    2: ['a', 'b', 'c'],
    3: ['d', 'e', 'f'],
    4: ['g', 'h', 'i'],
    5: ['j', 'k', 'l'],
    6: ['m', 'n', 'o'],
    7: ['p', 'q', 'r', 's'],
    8: ['t', 'u', 'v'],
    9: ['w', 'x', 'y', 'z'],
};

const multiple = function multi (arr) {
    if (arr.length === 0) {
        return []
    }
    if (arr.length === 1) {
        return arr[0]
    }

    if(arr.length === 2) {
        const newArr = [];
        const left = arr[0];
        const right = arr[1];
        left.forEach(l => right.forEach(r => newArr.push(l + r)));
        return newArr
    } else {
        const left = arr[0];
        const right = arr.slice(1);
        return multi([
            left,
            multi(right)
        ])
    }
};

const generateWord = (number) => {
    const arrayOfArrays = number.split('').map(num => digits[num])

    return multiple(arrayOfArrays)
};

module.exports = generateWord;
const wordList = require('./wordsList');

describe('wordList', () => {
    describe('Should handle N string length', () => {
        it('0', () => {
            const expected = [];
            const result = wordList('');

            expect(result).toEqual(expected);
        });

        it('1', () => {
            const expected = ['a', 'b', 'c', ];
            const result = wordList('2');

            expect(result).toEqual(expected);
        });

        it('2', () => {
            const expected = [
                'ad', 'ae', 'af',
                'bd', 'be', 'bf',
                'cd', 'ce', 'cf'
            ];
            const result = wordList('23');

            expect(result).toEqual(expected);
        });

        it('3', () => {
            const expected = [
                "add", "ade", "adf", "aed", "aee", "aef", "afd", "afe", "aff",
                "bdd", "bde", "bdf", "bed", "bee", "bef", "bfd", "bfe", "bff",
                "cdd", "cde", "cdf", "ced", "cee", "cef", "cfd", "cfe", "cff"
            ];
            const result = wordList('233');

            expect(result).toEqual(expected);
        });
    });
});
const express = require('express');
const router = express.Router();

const wordsController = require('../controllers/wordsController');

router.get('/words', wordsController.getWords);

module.exports = router;
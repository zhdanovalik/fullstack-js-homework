## Instalation
```bash
# npm
npm install && cd client/ && npm install && cd ..
```

## Running
```bash
# run backend and frontend together
npm start

# run backend
npm run server

# run frontend
npm run client

# additionaly, to run tests:
# backend
npm run test

# frontend
cd client/ && npm run test
```

## What's where
Based on:

* [Express application generator](https://expressjs.com/en/starter/generator.html)
* [Create React App](https://github.com/facebookincubator/create-react-app)

### Frontend

* `client/` - frontend folder

```
client/
├── * - project setup related files
├── public/ - static files
└── src
    ├── actions/ - redux actions
    ├── reducers/ - redux reducers
    ├── components/ - react components
    ├── styles/ - styled-components files
    ├── App.js - application root component
    └── index.js - application entry file
```

### Backend

* `/` - backend folder

```
/
├── * - project setup related files
├── client/ - frontend folder
├── controllers/ - frontend folder
├── handlers/ - utils modules
└── routes/ - routes handlers
```


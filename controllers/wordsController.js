const wordList = require('../handlers/wordsList');
const wordsArray = require('../handlers/10kWords.json');

const isQueryValid = (query) => {
    return query
        .split('')
        .map(item => Number(item))
        .every(digit => !isNaN(digit) && digit !== 1 && digit !== 0)
};

exports.getWords = (req, res) => {
    const query = req.query.q;

    if(!isQueryValid(query)) {
        return res.status(400).json({
            code: 400,
            message: 'Invalid query, accept only numbers 2-9'
        })
    }

    let words = wordList(query);

    // because we're not caching results and we don't have any pagination,
    // lest's filter out up to 100 words
    let matchedWords = [];
    for(let i=0; i < words.length; i++) {
        outer:
        for(let j=0; j < wordsArray.length; j++) {
            if(wordsArray[j].indexOf(words[i]) === 0) {
                if(matchedWords.length >= 100) {
                    break outer
                }
                matchedWords.push(wordsArray[j])
            }
        }
    }

    res.json({
        code: 200,
        query,
        count: matchedWords.length,
        items: matchedWords,
    })
};